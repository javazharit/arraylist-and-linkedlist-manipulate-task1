package me.babayan.test;

public class MyClass {

    private Integer n;

	/**
	 * Initializes a new instance of the class
	 * @param n
	 */
    public MyClass(Integer n) {
        this.n = n;
    }


	// Getter
	public Integer getN() {
		return n;
	}

	// Setter
	public void setN(Integer n) {
		this.n = n;
	}
}
