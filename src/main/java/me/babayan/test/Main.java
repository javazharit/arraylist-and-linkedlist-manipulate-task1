package me.babayan.test;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;

public class Main {

	public static void main(String[] args) throws Exception {
		Integer n;
		System.out.print("Please enter integer number: ");
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			n = Integer.parseInt(br.readLine());
			System.out.println("You entered the: " + n);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		int count = 0;
		int defaultCapacity = 10;

		ArrayList<MyClass> list = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			list.add(new MyClass(i));

			if (defaultCapacity != getCapacity(list)) {
				System.out.println("capacity: " + getCapacity(list));
				count++;
				System.out.println("count: " + count);
				defaultCapacity = getCapacity(list);
			}
		}
		System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
		System.out.println("ArrayList capacity was changed " + count + " times.");
	}

	/**
	 * return ArrayList capacity
	 */
	private static int getCapacity(ArrayList<?> i) throws Exception {
		Field dataField = ArrayList.class.getDeclaredField("elementData");
		dataField.setAccessible(true);
		return ((Object[]) dataField.get(i)).length;
	}

}
